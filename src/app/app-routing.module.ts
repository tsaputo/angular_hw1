import { WelcomePageComponent } from './welcome-page/welcome-page.component';
import { ContactsPageComponent } from './contacts-page/contacts-page.component';
import { GoalsPageComponent } from './goals-page/goals-page.component';
import { ExperiencePageComponent } from './experience-page/experience-page.component';
import { SkillsPageComponent } from './skills-page/skills-page.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: '', component: MainLayoutComponent, children: [
    {path: '', redirectTo: '/', pathMatch: 'full'},
    {path: '', component: WelcomePageComponent},
    {path: 'skills', component: SkillsPageComponent},
    {path: 'experience', component: ExperiencePageComponent},
    {path: 'goals', component: GoalsPageComponent},
    {path: 'contacts', component: ContactsPageComponent}
  ]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
