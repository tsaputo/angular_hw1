import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { SkillsPageComponent } from './skills-page/skills-page.component';
import { ExperiencePageComponent } from './experience-page/experience-page.component';
import { GoalsPageComponent } from './goals-page/goals-page.component';
import { ContactsPageComponent } from './contacts-page/contacts-page.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';

@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    SkillsPageComponent,
    ExperiencePageComponent,
    GoalsPageComponent,
    ContactsPageComponent,
    WelcomePageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
