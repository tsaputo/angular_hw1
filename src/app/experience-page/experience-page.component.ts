import { Component, OnInit } from '@angular/core';
import { DataService } from '../../app/data.service';

@Component({
  selector: 'app-experience-page',
  templateUrl: './experience-page.component.html',
  styleUrls: ['./experience-page.component.scss'],
  providers: [DataService]
})
export class ExperiencePageComponent implements OnInit {
  
  items: object[] = [];

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.items = this.dataService.getExperienceObject();
  }

}
