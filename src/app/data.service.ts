export class DataService{
  
  private data: object[] = [{
    'Programming languages': ['Javascript','Ruby']
  },{
    'Frameworks': ['React', 'Angular', 'Ruby on Rails']
  },{
    "RDBMS": ["Postgresql", "MySQL"]
  },{
    "Technologies": ["CSS/SCSS", "SASS/LESS", "webpacker/yarn", "Bootstrap"]
  },{
    'Methodology': ['BEM', 'Scrum'] 
  },{
    'Version Control': ['Git']
  },{
    'Image Editing Tools': ['Photoshop']
  },{
    'Libraries': ['JQuery']
  }];

  private experience: object[] = [
    {'Company': 'Softserve'},
    {'Job Position': 'Trainee Software Developer'},
    {'Project': 'Kanban board. Allows users to create and customise project boards \
    according to needs of the project. Projector allows to customise different access \
    levels for administrators, members and regular users. \
    Functionality includes creation of tasks, assignment them to members of the boards. \
    Authorisation is implemented using gem Device and Facebook.'},
    {'Participation':
    '- Design of DB structure \
    - Implementation of new features according to the requirements - Frontend and Backend development \
    - Requirements analysis and clarification \
    - Implementation of Unit tests using RSpec \
    - Participation in daily meetings, restrospectives, plannings'},
    {'Technologies': 'Ruby, RoR, JS, JQuery, Haml, RSpec, PostgreSql, Sidekiq'}
  ];
    
  getData(): object[] {
      return this.data;
  }

  getExperienceObject(): object[] {
    return this.experience;
  }
}