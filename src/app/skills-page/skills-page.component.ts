import { Component, OnInit } from '@angular/core';
import { DataService } from '../../app/data.service';


@Component({
  selector: 'app-skills-page',
  templateUrl: './skills-page.component.html',
  styleUrls: ['./skills-page.component.scss'],
  providers: [DataService]
})
export class SkillsPageComponent implements OnInit {

  items: object[] = [];

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.items = this.dataService.getData();
  }

}
